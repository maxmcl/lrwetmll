use std::mem;

pub struct List<T> {
    head: Link<T>,
}

enum Link<T> {
    Empty,
    More(Box<Node<T>>),
}

struct Node<T> {
    elem: T,
    next: Link<T>,
}

impl<T> List<T> {
    pub fn default() -> Self {
        Self { head: Link::Empty }
    }

    pub fn push(&mut self, t: T) {
        let new_node = Box::new(Node {
            elem: t,
            next: mem::replace(&mut self.head, Link::Empty),
        });
        self.head = Link::More(new_node);
    }

    pub fn pop(&mut self) -> Option<T> {
        match self.pop_node() {
            Link::Empty => None,
            Link::More(node) => Some(node.elem),
        }
    }

    fn pop_node(&mut self) -> Link<T> {
        match mem::replace(&mut self.head, Link::Empty) {
            Link::Empty => Link::Empty,
            Link::More(mut node) => {
                mem::swap(&mut self.head, &mut node.next);
                Link::More(node)
            }
        }
    }

    pub fn len(&self) -> usize {
        let mut len = 0;
        let mut link = &self.head;
        while let Link::More(node) = link {
            link = &node.next;
            len += 1
        }
        len
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

impl<T> Drop for List<T> {
    fn drop(&mut self) {
        while let Link::More(_) = self.pop_node() {}
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_first() {
        let mut list = List::<i32>::default();
        assert_eq!(list.pop(), None);
        assert_eq!(list.len(), 0);
        list.push(1);
        list.push(2);
        assert_eq!(list.len(), 2);
        assert_eq!(list.pop(), Some(2));
        assert_eq!(list.len(), 1);
        list.push(3);
        assert_eq!(list.len(), 2);
        assert_eq!(list.pop(), Some(3));
        assert_eq!(list.pop(), Some(1));
        assert_eq!(list.pop(), None);
        assert_eq!(list.len(), 0);
    }

    #[test]
    fn test_large() {
        let mut list = List::<i32>::default();
        for elem in 0..1e6 as i32 {
            list.push(elem);
        }
        assert_eq!(list.len(), 1e6 as usize);
        for _ in 0..1e6 as i32 {
            list.pop();
        }
        assert_eq!(list.len(), 0 as usize);
    }
}
