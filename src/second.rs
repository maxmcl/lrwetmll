use std::fmt::Debug;

pub struct List<T: Debug> {
    head: Link<T>,
}

type Link<T> = Option<Box<Node<T>>>;

#[derive(Debug)]
struct Node<T> {
    elem: T,
    next: Link<T>,
}

pub struct ListIntoIterator<T: Debug>(List<T>);

pub struct ListIterator<'a, T: Debug> {
    next: Option<&'a Node<T>>,
}

pub struct ListMutIterator<'a, T: Debug> {
    next: Option<&'a mut Node<T>>,
}

impl<T: Debug> List<T> {
    pub fn default() -> Self {
        Self { head: None }
    }

    pub fn push(&mut self, t: T) {
        let new_node = Box::new(Node {
            elem: t,
            next: self.head.take(),
        });
        self.head = Some(new_node);
    }

    fn pop(&mut self) -> Option<T> {
        self.head.take().map(|node| {
            self.head = node.next;
            node.elem
        })
    }

    pub fn len(&self) -> usize {
        let mut len = 0;
        let mut link = &self.head;
        while let Some(node) = link {
            link = &node.next;
            len += 1
        }
        len
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn peek(&self) -> Option<&T> {
        self.head.as_ref().map(|node| &node.elem)
    }

    pub fn peek_mut(&mut self) -> Option<&mut T> {
        self.head.as_mut().map(|node| &mut node.elem)
    }

    pub fn insert(&mut self, t: T, index: usize) {
        let mut depth = 1;
        let mut prev_link = None;
        let mut curr_link = self.head.as_deref_mut();
        while let Some(node) = curr_link.take() {
            match depth {
                depth if depth == index => {
                    // The next node is the insertion site
                    // The current link points to the new node
                    // The new node points to the current's next
                    node.next = Some(Box::new(Node {
                        elem: t,
                        next: node.next.take(),
                    }));
                    return;
                }
                depth if depth > index => {
                    break;
                }
                _ => (),
            }
            prev_link = curr_link.take();
            curr_link = node.next.as_deref_mut();
            depth += 1;
        }
        // The list was shorter than the request index, append at the end
        match prev_link {
            None => self.push(t),
            Some(node) => {
                node.next = Some(Box::new(Node {
                    elem: t,
                    next: None,
                }))
            }
        }
    }
}

impl<T: Debug> IntoIterator for List<T> {
    type Item = T;
    type IntoIter = ListIntoIterator<T>;
    fn into_iter(self) -> Self::IntoIter {
        ListIntoIterator(self)
    }
}

impl<'a, T: Debug> IntoIterator for &'a List<T> {
    type Item = &'a T;
    type IntoIter = ListIterator<'a, T>;
    fn into_iter(self) -> Self::IntoIter {
        ListIterator {
            next: self.head.as_deref(),
        }
    }
}

impl<T: Debug> Drop for List<T> {
    fn drop(&mut self) {
        while self.pop().is_some() {}
    }
}

impl<'a, T: Debug> Iterator for ListIterator<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        self.next.map(|node| {
            self.next = node.next.as_deref();
            &node.elem
        })
    }
}

impl<'a, T: Debug> Iterator for ListMutIterator<'a, T> {
    type Item = &'a mut T;
    fn next(&mut self) -> Option<Self::Item> {
        self.next.take().map(|node| {
            self.next = node.next.as_deref_mut();
            &mut node.elem
        })
    }
}

impl<T: Debug> Iterator for ListIntoIterator<T> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.pop()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_first() {
        let mut list = List::<i32>::default();
        assert_eq!(list.pop(), None);
        assert_eq!(list.len(), 0);
        list.push(1);
        list.push(2);
        assert_eq!(list.len(), 2);
        assert_eq!(list.pop(), Some(2));
        assert_eq!(list.len(), 1);
        list.push(3);
        assert_eq!(list.len(), 2);
        assert_eq!(list.pop(), Some(3));
        assert_eq!(list.pop(), Some(1));
        assert_eq!(list.pop(), None);
        assert_eq!(list.len(), 0);
    }

    #[test]
    fn test_large() {
        let mut list = List::<i32>::default();
        for elem in 0..1e6 as i32 {
            list.push(elem);
        }
        assert_eq!(list.len(), 1e6 as usize);
        for _ in 0..1e6 as i32 {
            list.pop();
        }
        assert_eq!(list.len(), 0 as usize);
    }

    #[test]
    fn test_peek() {
        let mut list = List::<i32>::default();
        assert_eq!(list.peek(), None);
        assert_eq!(list.peek_mut(), None);
        list.push(0);
        assert_eq!(list.peek(), Some(&0));
        assert_eq!(list.peek_mut(), Some(&mut 0));
        let peeked = list.peek_mut();
        peeked.map(|node| *node = 1);
        assert_eq!(list.peek(), Some(&1));
    }

    #[test]
    fn test_into_iter() {
        let mut list = List::<i32>::default();
        list.push(1);
        list.push(2);
        list.push(3);
        let mut iter = list.into_iter();
        assert_eq!(iter.next(), Some(3));
        assert_eq!(iter.next(), Some(2));
        assert_eq!(iter.next(), Some(1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_iter() {
        let mut list = List::<i32>::default();
        list.push(1);
        list.push(2);
        list.push(3);
        // Not sure about this (&list).into_iter thing to call the ref form???
        let mut iter: ListIterator<i32> = (&list).into_iter();
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_insert() {
        let mut list = List::<i32>::default();
        list.insert(1, 1);
        list.insert(2, 1);
        list.insert(3, 1);
        assert_eq!(list.len(), 3);
        assert_eq!(list.pop(), Some(1));
        assert_eq!(list.pop(), Some(3));
        assert_eq!(list.pop(), Some(2));
        assert_eq!(list.pop(), None);
    }
}
