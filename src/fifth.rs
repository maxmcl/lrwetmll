pub struct List<T> {
    head: Link<T>,
    tail: *mut Node<T>,
}

type Link<T> = *mut Node<T>;

struct Node<T> {
    elem: T,
    next: Link<T>,
}

impl<T> List<T> {
    pub fn new() -> Self {
        Self {
            head: std::ptr::null_mut(),
            tail: std::ptr::null_mut(),
        }
    }

    pub fn push(&mut self, elem: T) {
        let new_tail = Box::into_raw(Box::new(Node {
            elem,
            next: std::ptr::null_mut(),
        }));
        match self.tail.is_null() {
            true => {
                // The list is empty, assign the new tail to the head
                self.head = new_tail;
            }
            false => {
                // The list is non empty, assign the prev tail to the new tail
                unsafe {
                    (*self.tail).next = new_tail;
                }
            }
        }
        // Make the tail point to the head
        self.tail = new_tail;
    }

    pub fn pop(&mut self) -> Option<T> {
        if !self.head.is_null() {
            let temp = unsafe { Box::from_raw(self.head) };
            self.head = temp.next;
            if self.head.is_null() {
                self.tail = std::ptr::null_mut();
            }
            Some(temp.elem)
        } else {
            None
        }
    }

    pub fn peek(&self) -> Option<&T> {
        unsafe { self.head.as_ref() }.map(|node| &node.elem)
    }
}

impl<T> Default for List<T> {
    fn default() -> Self {
        Self::new()
    }
}

pub struct ListIntoIterator<T>(List<T>);

impl<T> Iterator for ListIntoIterator<T> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.pop()
    }
}

impl<T> IntoIterator for List<T> {
    type Item = T;
    type IntoIter = ListIntoIterator<T>;
    fn into_iter(self) -> Self::IntoIter {
        ListIntoIterator(self)
    }
}

pub struct ListIterator<'a, T> {
    next: *mut Node<T>,
    phantom: std::marker::PhantomData<&'a T>,
}

impl<'a, T> Iterator for ListIterator<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        unsafe { self.next.as_ref() }.map(|node| {
            self.next = node.next;
            &node.elem
        })
    }
}

impl<'a, T> IntoIterator for &'a List<T> {
    type Item = &'a T;
    type IntoIter = ListIterator<'a, T>;
    fn into_iter(self) -> Self::IntoIter {
        ListIterator {
            next: self.head,
            phantom: std::marker::PhantomData,
        }
    }
}

impl<T> Drop for List<T> {
    fn drop(&mut self) {
        while self.pop().is_some() {}
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test() {
        let mut list: List<u8> = List::default();
        list.push(1);
        list.push(2);
        list.push(3);
        assert_eq!(list.pop(), Some(1));
        assert_eq!(list.pop(), Some(2));
        list.push(4);
        list.push(5);
        assert_eq!(list.pop(), Some(3));
        assert_eq!(list.pop(), Some(4));
        assert_eq!(list.pop(), Some(5));
        assert_eq!(list.pop(), None);
        list.push(6);
        assert_eq!(list.pop(), Some(6));
        assert_eq!(list.pop(), None);
    }

    #[test]
    fn test_into_iter() {
        let mut list: List<u8> = List::default();
        list.push(1);
        list.push(2);
        list.push(3);
        let mut iter = list.into_iter();
        assert_eq!(iter.next(), Some(1));
        assert_eq!(iter.next(), Some(2));
        assert_eq!(iter.next(), Some(3));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_iter() {
        let mut list: List<u8> = List::default();
        list.push(1);
        list.push(2);
        list.push(3);
        let mut iter = (&list).into_iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_peek() {
        let mut list: List<u8> = List::default();
        assert_eq!(list.peek(), None);
        list.push(1);
        assert_eq!(list.peek(), Some(&1));
    }
}
