use std::rc::Rc;

pub struct List<T> {
    head: Link<T>,
}

type Link<T> = Option<Rc<Node<T>>>;

pub struct Node<T> {
    elem: T,
    next: Link<T>,
}

impl<T> Default for List<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> List<T> {
    pub fn new() -> Self {
        Self { head: None }
    }

    pub fn push(&mut self, t: T) {
        self.head = Some(Rc::new(Node {
            elem: t,
            next: self.head.take(),
        }));
    }

    pub fn prepend(&self, t: T) -> Self {
        Self {
            head: Some(Rc::new(Node {
                elem: t,
                next: self.head.clone(),
            })),
        }
    }

    pub fn tail(&self) -> Self {
        Self {
            head: self.head.as_ref().and_then(|node| node.next.clone()),
        }
    }

    pub fn head(&self) -> Option<&T> {
        self.head.as_deref().map(|node| &node.elem)
    }

    pub fn pop(&mut self) -> Option<T> {
        self.head
            .take()
            .and_then(|node| match Rc::try_unwrap(node) {
                Ok(n) => {
                    self.head = n.next;
                    Some(n.elem)
                }
                Err(_) => None,
            })
    }
}

impl<T> Drop for List<T> {
    fn drop(&mut self) {
        let mut link = self.head.take();
        while let Some(node) = link {
            match Rc::try_unwrap(node) {
                Ok(mut n) => link = n.next.take(),
                Err(_) => break,
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_list() {
        let mut list = List::new();
        assert_eq!(list.head(), None);
        list = list.prepend(1).prepend(2).prepend(3);
        assert_eq!(list.head(), Some(&3));
        list = list.tail();
        assert_eq!(list.head(), Some(&2));
        list = list.tail();
        assert_eq!(list.head(), Some(&1));
        list = list.tail();
        assert_eq!(list.head(), None);
    }

    #[test]
    fn test_push_pop() {
        let mut list = List::new();
        list.push(1);
        assert_eq!(list.head(), Some(&1));
        list.push(2);
        assert_eq!(list.head(), Some(&2));
        assert_eq!(list.pop(), Some(2));
        assert_eq!(list.pop(), Some(1));
        assert_eq!(list.pop(), None);
    }
}
