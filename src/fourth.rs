use std::cell::{Ref, RefCell, RefMut};
use std::rc::Rc;

pub struct List<T> {
    head: Link<T>,
    tail: Link<T>,
}

type Link<T> = Option<Rc<RefCell<Node<T>>>>;

pub struct Node<T> {
    elem: T,
    prev: Link<T>,
    next: Link<T>,
}

impl<T> Default for List<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> Node<T> {
    pub fn new(elem: T) -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Self {
            elem,
            prev: None,
            next: None,
        }))
    }
}

impl<T> List<T> {
    pub fn new() -> Self {
        Self {
            head: None,
            tail: None,
        }
    }

    pub fn push_front(&mut self, elem: T) {
        // head becomes a new node that stores elem
        // new head's next is the head
        // head's prev is the new head
        let new_head = Node::new(elem);
        match self.head.take() {
            Some(head) => {
                new_head.borrow_mut().next = Some(head.clone());
                head.borrow_mut().prev = Some(new_head.clone());
                self.head = Some(new_head);
            }
            None => {
                self.tail = Some(new_head.clone());
                self.head = Some(new_head);
            }
        }
    }

    pub fn pop_front(&mut self) -> Option<T> {
        // head is returned
        // next node becomes head
        // next node previous becomes None
        self.head.take().and_then(|node| {
            match node.borrow_mut().next.take() {
                Some(new_head) => {
                    new_head.borrow_mut().prev = None;
                    self.head = Some(new_head);
                }
                None => {
                    // There is no next node -- the list is now empty
                    self.tail = None;
                }
            }
            Rc::try_unwrap(node).ok().map(|node| node.into_inner().elem)
        })
    }

    pub fn push_back(&mut self, elem: T) {
        let new_tail = Node::new(elem);
        match self.tail.take() {
            Some(tail) => {
                new_tail.borrow_mut().prev = Some(tail.clone());
                tail.borrow_mut().next = Some(new_tail.clone());
                self.tail = Some(new_tail);
            }
            None => {
                self.tail = Some(new_tail.clone());
                self.head = Some(new_tail);
            }
        }
    }

    pub fn pop_back(&mut self) -> Option<T> {
        self.tail.take().and_then(|node| {
            match node.borrow_mut().prev.take() {
                Some(new_tail) => {
                    new_tail.borrow_mut().next = None;
                    self.tail = Some(new_tail);
                }
                None => {
                    self.head = None;
                }
            }
            Rc::try_unwrap(node).ok().map(|node| node.into_inner().elem)
        })
    }

    pub fn head(&self) -> Option<Ref<T>> {
        self.head
            .as_ref()
            .map(|node| Ref::map(node.borrow(), |node| &node.elem))
    }

    pub fn tail(&self) -> Option<Ref<T>> {
        self.tail
            .as_ref()
            .map(|node| Ref::map(node.borrow(), |node| &node.elem))
    }

    pub fn head_mut(&mut self) -> Option<RefMut<T>> {
        self.head
            .as_ref()
            .map(|node| RefMut::map(node.borrow_mut(), |node| &mut node.elem))
    }

    pub fn tail_mut(&mut self) -> Option<RefMut<T>> {
        self.tail
            .as_ref()
            .map(|node| RefMut::map(node.borrow_mut(), |node| &mut node.elem))
    }
}

impl<T> Drop for List<T> {
    fn drop(&mut self) {
        while self.pop_front().is_some() {}
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_list_front() {
        let mut list = List::new();
        list.push_front(1);
        list.push_front(2);
        list.push_front(3);

        assert_eq!(list.head().map(|value| *value), Some(3));
        assert_eq!(list.pop_front(), Some(3));
        assert_eq!(*list.head().unwrap(), 2);
        assert_eq!(list.pop_front(), Some(2));
        assert_eq!(*list.head().unwrap(), 1);
        assert_eq!(list.pop_front(), Some(1));
        assert!(list.head().is_none());
        assert_eq!(list.pop_front(), None);
    }

    #[test]
    fn test_list_back() {
        let mut list = List::new();
        list.push_back(3);
        list.push_back(2);
        list.push_back(1);

        assert_eq!(list.tail().map(|value| *value), Some(1));
        assert_eq!(list.pop_back(), Some(1));
        assert_eq!(*list.head().unwrap(), 3);
        assert_eq!(list.pop_front(), Some(3));
        assert_eq!(*list.tail().unwrap(), 2);
        assert_eq!(list.pop_back(), Some(2));
        assert!(list.head().is_none());
        assert!(list.tail().is_none());
        assert_eq!(list.pop_front(), None);
        assert_eq!(list.pop_back(), None);
    }

    #[test]
    fn test_mut() {
        let mut list = List::new();
        list.push_front(1);

        assert_eq!(&mut *list.head_mut().unwrap(), &mut 1);
    }
}
